import { html, render } from "https://unpkg.com/lit-html?module"
import tabContainer from "./tab-container.js"

window.onload = eve => {
	const data = {
		active_tab: null,
		title: "test",
		color: "#333",
		text: "message",
	}

	const update = () =>
		render(
			html`
				<h1>${data.title}</h1>
				${tabContainer(data, update)}
			`,
			document.body
		)
	
	update()
}
