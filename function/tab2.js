import { html } from "https://unpkg.com/lit-html?module"
export default (data, update, tab_data) => {
	setTimeout(update, 1000)

	return html`
		<div data-tab-name="tab2">
			<h2>tab2</h2>
			<p>${new Date().toLocaleString()}</p>
		</div>
	`
}
