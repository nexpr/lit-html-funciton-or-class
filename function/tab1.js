import { html } from "https://unpkg.com/lit-html?module"
export default (data, update, tab_data) => {
	return html`
		<div data-tab-name="tab1">
			<h2>tab1</h2>
			<p style=${`color: ${data.color}`}>${data.text}</p>
		</div>
	`
}
