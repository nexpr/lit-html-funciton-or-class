import { html } from "https://unpkg.com/lit-html?module"
import BaseClass from "./base-class.js"

export default class extends BaseClass {
	render() {
		return html`
			<div data-tab-name="tab1">
				<h2>tab1</h2>
				<p style=${`color: ${this.data.color}`}>${this.data.text}</p>
			</div>
		`
	}
}
