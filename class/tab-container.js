import { html } from "https://unpkg.com/lit-html?module"
import BaseClass from "./base-class.js"
import tab1 from "./tab1.js"
import tab2 from "./tab2.js"
import option from "./option.js"

const tabs = { tab1, tab2, option }

export default class extends BaseClass {
	constructor(...a) {
		super(...a)
		if (!this.data.active_tab) {
			this.data.active_tab = { name: "tab1", instance: new tab1(this.data, this.update) }
		}
	}

	get handlers() {
		return {
			switchTab: eve => {
				const elem = eve.target.closest("[data-tab]")
				const cls = tabs[elem.dataset.tab]
				this.data.active_tab = { name: elem.dataset.tab, instance: new cls(this.data, this.update) }
				this.update()
			},
		}
	}

	render() {
		const data = this.data
		const tab_list = ["tab1", "tab2", "option"]

		return html`
			<div class="tab-container">
				<div class="tab-head-container" @click=${this.handlers.switchTab}>
					${tab_list.map(e => {
						return html`
							<div class=${`tab-head ${data.active_tab.name === e ? "active" : ""}`} data-tab=${e}>
								${e}
							</div>
						`
					})}
				</div>
				<div class="tab-body-container">
					${data.active_tab.instance.render()}
				</div>
			</div>
		`
	}
}
