import { html } from "https://unpkg.com/lit-html?module"
import BaseClass from "./base-class.js"

export default class extends BaseClass {
	render() {
		setTimeout(this.update, 1000)

		return html`
			<div data-tab-name="tab2">
				<h2>tab2</h2>
				<p>${new Date().toLocaleString()}</p>
			</div>
		`
	}
}
