import { html } from "https://unpkg.com/lit-html?module"

export default class BaseClass {
	constructor(data, update) {
		this.data = data
		this.update = update
	}

	render() {
		return html``
	}
}
