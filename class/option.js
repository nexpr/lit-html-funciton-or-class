import { html } from "https://unpkg.com/lit-html?module"
import BaseClass from "./base-class.js"

export default class extends BaseClass {
	constructor(...a) {
		super(...a)
		this.editing = {
			title: this.data.title,
			color: this.data.color,
			text: this.data.text,
		}
	}

	get handlers() {
		return {
			input: eve => {
				const name = eve.target.name
				if (!name) return
				this.editing[name] = eve.target.value
				this.update()
			},
			save: eve => {
				this.data.title = this.editing.title
				this.data.color = this.editing.color
				this.data.text = this.editing.text
				this.update()
			},
		}
	}

	render() {
		return html`
			<div data-tab-name="option">
				<h2>option</h2>
				<table @input=${this.handlers.input}>
					<tr>
						<th>Title</th>
						<td><input name="title" .value=${this.editing.title}></td>
					</tr>
					<tr>
						<th>Color</th>
						<td><input name="color" .value=${this.editing.color}></td>
					</tr>
					<tr>
						<th>Text</th>
						<td><input name="text" .value=${this.editing.text}></td>
					</tr>
				</table>
				<button @click=${this.handlers.save}>Save</button>
			</div>
		`
	}
}
