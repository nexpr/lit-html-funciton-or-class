import { html } from "https://unpkg.com/lit-html?module"
import component from "./component.js"

export default component("tab2", (host, ref, data, update) => {
	setTimeout(update, 1000)

	return html`
		<div data-tab-name="tab2">
			<h2>tab2</h2>
			<p>${new Date().toLocaleString()}</p>
		</div>
	`
})
