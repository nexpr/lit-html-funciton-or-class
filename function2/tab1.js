import { html } from "https://unpkg.com/lit-html?module"
import component from "./component.js"

export default component("tab1", (host, ref, data, update) => {
	return html`
		<div data-tab-name="tab1">
			<h2>tab1</h2>
			<p style=${`color: ${data.color}`}>${data.text}</p>
		</div>
	`
})
