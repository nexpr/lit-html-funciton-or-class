import { html } from "https://unpkg.com/lit-html?module"
import tab1 from "./tab1.js"
import tab2 from "./tab2.js"
import option from "./option.js"
import component from "./component.js"

const tabs = { tab1, tab2, option }

export default component("tab-container", (host, ref, data, update) => {
	if (!data.active_tab) {
		data.active_tab = { name: "tab1", data: {} }
	}

	const h = {
		switchTab(eve) {
			const elem = eve.target.closest("[data-tab]")
			data.active_tab = { name: elem.dataset.tab, data: {} }
			update()
		},
	}

	const tab_list = ["tab1", "tab2", "option"]

	return html`
		<div class="tab-container">
			<div class="tab-head-container" @click=${h.switchTab}>
				${tab_list.map(e => {
					return html`
						<div class=${`tab-head ${data.active_tab.name === e ? "active" : ""}`} data-tab=${e}>
							${e}
						</div>
					`
				})}
			</div>
			<div class="tab-body-container">
				${tabs[data.active_tab.name](data.active_tab.data)(data, update)}
			</div>
		</div>
	`
})
