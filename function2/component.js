import { render } from "https://unpkg.com/lit-html?module"

const store = {}
const elemstore = (component_name, ref) => {
	const wmap = (store[component_name] = store[component_name] || new WeakMap())
	if (!wmap.has(ref)) {
		const element = document.createElement("div")
		element.className = "component-host"
		element.dataset.component = component_name
		wmap.set(ref, element)
	}
	const element = wmap.get(ref)
	return element
}

export default (component_name, template) => ref => (...args) => {
	const host = elemstore(component_name, ref)
	render(template(host, ref, ...args), host)
	return host
}
