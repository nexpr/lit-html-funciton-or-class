import { html } from "https://unpkg.com/lit-html?module"
import component from "./component.js"

export default component("option", (host, ref, data, update) => {
	const editing = (ref.editing = ref.editing || {
		title: data.title,
		color: data.color,
		text: data.text,
	})

	const h = {
		input(eve) {
			const name = eve.target.name
			if (!name) return
			editing[name] = eve.target.value
			update()
		},
		save(eve) {
			data.title = editing.title
			data.color = editing.color
			data.text = editing.text
			update()
		},
	}

	return html`
		<div data-tab-name="option">
			<h2>option</h2>
			<table @input=${h.input}>
				<tr>
					<th>Title</th>
					<td><input name="title" .value=${editing.title} /></td>
				</tr>
				<tr>
					<th>Color</th>
					<td><input name="color" .value=${editing.color} /></td>
				</tr>
				<tr>
					<th>Text</th>
					<td><input name="text" .value=${editing.text} /></td>
				</tr>
			</table>

			<button @click=${h.save}>Save</button>
		</div>
	`
})
