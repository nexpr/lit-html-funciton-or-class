import { html, render } from "https://unpkg.com/lit-html?module"
import TabContainer from "./tab-container.js"

window.onload = eve => {
	const data = {
		active_tab: null,
		title: "test",
		color: "#333",
		text: "message",
	}

	const update = () =>
		render(
			html`
				<h1>${data.title}</h1>
				${tab_container.render()}
			`,
			document.body
		)

	const tab_container = new TabContainer(data, update)
	update()
}
