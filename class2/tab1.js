import { html } from "https://unpkg.com/lit-html?module"
import BaseClass from "./base-class.js"

export default class extends BaseClass {
	get component_name() {
		return "tab1"
	}

	get template() {
		return html`
			<div data-tab-name="tab1">
				<h2>tab1</h2>
				<p style=${`color: ${this.data.color}`}>${this.data.text}</p>
			</div>
		`
	}
}
