import { html } from "https://unpkg.com/lit-html?module"
import BaseClass from "./base-class.js"

export default class extends BaseClass {
	get component_name() {
		return "tab2.js"
	}

	get template() {
		setTimeout(this.update, 1000)

		return html`
			<div data-tab-name="tab2">
				<h2>tab2</h2>
				<p>${new Date().toLocaleString()}</p>
			</div>
		`
	}
}
