import { html, render } from "https://unpkg.com/lit-html?module"

export default class BaseClass {
	constructor(data, update) {
		this.data = data
		this.update = update
		this.root = this.createRoot()
	}

	createRoot() {
		const element = document.createElement("div")
		element.className = "component-host"
		element.dataset.component = this.component_name
		return element
	}

	get component_name() {
		return ""
	}

	get template() {
		return html``
	}

	render() {
		render(this.template, this.root)
		return this.root
	}
}
